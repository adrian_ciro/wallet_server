package com.wallet.domain;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.io.Serializable;
import lombok.*;

/**
 * A UserEntity.
 */
@Entity
@Table(name = "user")
@Getter
@Setter
@NoArgsConstructor
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    @Column(nullable = false)
    private String username;

    @NotNull
    @Column(nullable = false)
    private Double balance;

    @NotNull
    @Column(name = "balance_version", nullable = false)
    private Long balanceVersion;

    @Override
    public String toString() {
        return "UserEntity{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", balance=" + balance +
                ", balanceVersion=" + balanceVersion +
                '}';
    }
}
