package com.wallet.domain;

import com.wallet.domain.dto.TransactionDto;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by adrian on 11/09/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class InMemoryDb {

    private Long balanceVersion;

    private double balance;

    private boolean isSynchronized;

    @SuppressWarnings("unchecked")
    private Map<Long, TransactionDto> transactions =  new HashMap();
}
