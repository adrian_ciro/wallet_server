package com.wallet.domain;

/**
 * Created by adrian on 09/08/17.
 */
public class Errors {

    public static final String NO_ERROR = "";
    public static final String ERROR_01 = "Balance Limit exceeded";
    public static final String ERROR_02 = "Balance is negative";
    public static final String ERROR_03 = "User is in the black list";
    public static final String ERROR_04 = "User not found";
    public static final String ERROR_05 = "Transaction is duplicated";
    public static final String ERROR_06 = "Transaction ID exists but is not duplicated";

}
