package com.wallet.domain.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

/**
 * Created by adrian on 09/08/17.
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class TransactionDto {

    private Long transactionId;

    @JsonIgnore
    private String username;

    private Long balanceVersion;

    private Double balanceChange;

    private Double newBalance;


}
