package com.wallet.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by adrian on 09/08/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class ResponseDto extends TransactionDto {

    private String error;
}
