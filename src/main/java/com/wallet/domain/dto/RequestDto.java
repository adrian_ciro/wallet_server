package com.wallet.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by adrian on 09/08/17.
 */
@Getter
@Setter
@AllArgsConstructor
public class RequestDto {

    @NotNull
    private Long transactionId;

    @NotBlank
    private String username;

    @NotNull
    private Double value;
}
