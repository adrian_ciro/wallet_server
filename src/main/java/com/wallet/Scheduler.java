package com.wallet;

import com.wallet.domain.UserEntity;
import com.wallet.repository.UserRepository;
import lombok.extern.java.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import static com.wallet.Application.transactionsInMemory;

/**
 * Class responsible for storing the data from memory to db
 */
@Component
@Log
public class Scheduler {

    public static final int FIXED_BACKUP_EXECUTION_MS = 30000;

    @Autowired
    private UserRepository userRepository;

    @Scheduled(fixedDelay = FIXED_BACKUP_EXECUTION_MS)
    private void runDbBackUps() {

        log.info("Initiating automatic back up of records into database");

        transactionsInMemory.values().stream().filter(inMemoryDb -> !inMemoryDb.isSynchronized()).forEach(inMemoryDb -> {
            String username = inMemoryDb.getTransactions().values().stream().findAny().get().getUsername();
            UserEntity user = userRepository.findByUsername(username).get(0);
            user.setBalance(inMemoryDb.getBalance());
            user.setBalanceVersion(inMemoryDb.getBalanceVersion());
            userRepository.save(user);
            inMemoryDb.setSynchronized(true);
            log.info("Information of user "+username+" was updated in database");
        });
    }

}
