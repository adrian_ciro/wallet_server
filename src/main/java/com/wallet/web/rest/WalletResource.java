package com.wallet.web.rest;

import com.wallet.domain.Errors;
import com.wallet.domain.dto.RequestDto;
import com.wallet.domain.dto.ResponseDto;
import com.wallet.service.WalletService;
import lombok.extern.java.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URISyntaxException;

/**
 * REST controller for managing UserEntity.
 */
@RestController
@RequestMapping("/api")
@Log
public class WalletResource {

    @Autowired
    private WalletService walletService;

    @PostMapping("/updateBalance")
    public ResponseEntity<ResponseDto> createWorkout(@Valid @RequestBody RequestDto request) throws URISyntaxException {

        String error = walletService.checkBusinessRules(request);
        if (!Errors.NO_ERROR.equals(error)) {
            log.info("Exception for transaction ID: "+request.getTransactionId()+ " with error "+error+ " for username "+request.getUsername());
            return ResponseEntity.ok(createResponseWithError(request.getTransactionId(), error));
        }
        ResponseDto result = walletService.createResponse(request);
        return ResponseEntity.ok(result);
    }

    private ResponseDto createResponseWithError(Long transactionId, String error) {
        ResponseDto responseDto =  new ResponseDto(error);
        responseDto.setTransactionId(transactionId);
        return responseDto;
    }

}
