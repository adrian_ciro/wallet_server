package com.wallet.repository;

import com.wallet.domain.UserEntity;
import org.springframework.data.jpa.repository.*;
import java.util.List;

/**
 * Spring Data JPA repository for the UserEntity entity.
 */
@SuppressWarnings("unused")
public interface UserRepository extends JpaRepository<UserEntity,Long> {

    List<UserEntity> findByUsername(String username);

}
