package com.wallet.repository;

import com.wallet.domain.BannedUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;


/**
 * Spring Data JPA repository for the UserEntity entity.
 */
@SuppressWarnings("unused")
public interface BannedUserRepository extends JpaRepository<BannedUserEntity,Long> {

}
