package com.wallet.service;

import com.wallet.domain.Errors;
import com.wallet.domain.InMemoryDb;
import com.wallet.domain.UserEntity;
import com.wallet.domain.dto.RequestDto;
import com.wallet.domain.dto.ResponseDto;
import com.wallet.domain.dto.TransactionDto;
import com.wallet.repository.BannedUserRepository;
import com.wallet.repository.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

import static com.wallet.Application.transactionsInMemory;

/**
 * Service Implementation for managing UserEntity.
 */
@Service
@Transactional
@Slf4j
public class WalletService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private BannedUserRepository bannedUserRepository;

    @Value("${wallet.limit}")
    private Double limit;

    public String checkBusinessRules(RequestDto requestDto) {

        if(isUserNotFound(requestDto.getUsername())){
            log.info("User "+ requestDto.getUsername() + "not found and transaction ID: "+requestDto.getTransactionId());
            return Errors.ERROR_04;
        }

        if(requestDto.getValue()>limit){
            log.info("Transaction request exceeds the limit. RequestID: "+requestDto.getTransactionId()+ " for username "+requestDto.getUsername());
            return Errors.ERROR_01;
        }

        if(isUserBanned(requestDto.getUsername())){
            log.info("The user "+ requestDto.getUsername() +" is banned. Operation denied.");
            return Errors.ERROR_03;
        }

        return Errors.NO_ERROR;
    }

    public ResponseDto createResponse(RequestDto requestDto) {
        TransactionDto transaction;
        Map<Long, TransactionDto> transactionsMap;
        ResponseDto responseDto =  new ResponseDto("");
        responseDto.setTransactionId(requestDto.getTransactionId());

        //First initialize inMemory balances
        if(!transactionsInMemory.containsKey(requestDto.getUsername())){
            //user balance is not in memory
            UserEntity user = userRepository.findByUsername(requestDto.getUsername()).get(0);
            transactionsMap = new HashMap<>();
            transactionsInMemory.put(requestDto.getUsername(),
                    new InMemoryDb(user.getBalanceVersion(), user.getBalance(), true, transactionsMap));
        }


        //Second Check if transaction is duplicated
        if(isTransactionDuplicated(requestDto)){
           transaction = transactionsInMemory.get(requestDto.getUsername()).getTransactions().get(requestDto.getTransactionId());
           responseDto.setBalanceVersion(transaction.getBalanceVersion());
           responseDto.setBalanceChange(transaction.getBalanceChange());
           responseDto.setNewBalance(transaction.getNewBalance());

           log.info("Transaction ID: "+requestDto.getTransactionId()+ " is duplicated for username "+requestDto.getUsername());
           return responseDto;
        }


        //Third, check for balance is greater than 0
        InMemoryDb inMemoryDb = transactionsInMemory.get(requestDto.getUsername());
        if(inMemoryDb.getBalance()-requestDto.getValue()<0){
            responseDto.setError(Errors.ERROR_02);
            log.info("Balance can not be updated because balance would be negative for username "+requestDto.getUsername() + ". TransactionID: " +requestDto.getTransactionId());
            return responseDto;
        }

        //Otherwise, make the update
        inMemoryDb.setBalance(inMemoryDb.getBalance()-requestDto.getValue());
        inMemoryDb.setBalanceVersion(inMemoryDb.getBalanceVersion()+1L);
        inMemoryDb.setSynchronized(false);
        transactionsMap = inMemoryDb.getTransactions();
        transactionsMap.put(requestDto.getTransactionId(),
                new TransactionDto(requestDto.getTransactionId(),
                        requestDto.getUsername(),
                        inMemoryDb.getBalanceVersion(),
                        requestDto.getValue(),
                        inMemoryDb.getBalance()));

        responseDto.setBalanceVersion(inMemoryDb.getBalanceVersion());
        responseDto.setBalanceChange(requestDto.getValue());
        responseDto.setNewBalance(inMemoryDb.getBalance());

        log.info("Balance updated for transaction ID: "+requestDto.getTransactionId()+ " for username "+requestDto.getUsername());
        return responseDto;
    }

    private boolean isTransactionDuplicated(RequestDto requestDto) {

        Set<Long> keySet = transactionsInMemory.get(requestDto.getUsername()).getTransactions().keySet();
        if(keySet.size()>1000){
            //Take the latest 1000
            List reverse = keySet.stream().sorted().collect(Collectors.toList());
            Collections.reverse(reverse);
            reverse = reverse.subList(0,1000);
            return reverse.contains(requestDto.getTransactionId());
        }

        return transactionsInMemory.get(requestDto.getUsername()).getTransactions().containsKey(requestDto.getTransactionId());
    }

    private boolean isUserNotFound(String username) {
        return  userRepository.findByUsername(username).isEmpty();
    }

    private boolean isUserBanned(String username) {
        return bannedUserRepository.findAll().stream()
                .filter(bannedUserEntity -> bannedUserEntity.getUsername().equalsIgnoreCase(username)).findAny().isPresent();
    }
}
