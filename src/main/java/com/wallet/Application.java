package com.wallet;

import com.wallet.domain.InMemoryDb;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.convert.threeten.Jsr310JpaConverters;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
@EntityScan(basePackageClasses = { Application.class, Jsr310JpaConverters.class })
@EnableJpaRepositories(basePackages = {"com.wallet.repository"})
@EnableScheduling
public class Application {

	public static Map<String, InMemoryDb> transactionsInMemory = new HashMap<>();

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}
}
