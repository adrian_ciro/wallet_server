#BACKEND DEVELOPMENT FOR WALLET

## How to execute it

1. Build this project
```
gradle build
```
2. Run the application

```
gradle bootRun
```


##Description 


Server offers service for player wallet (balance). Wallet state (balance) should be managed in memory (3rd party solution may not be used). Balance is backed up in database (hsql). When balance is not in memory, it is loaded from database and any changes are done in memory. 
Player record in database is created on demand. There is a periodical background process to write changes from memory to database.


Constraints on balance changes:

1.       Balance cannot be less than 0.

2.       If transaction exists (is duplicate), then previous response is returned. Check may take into consideration only 1000 latest transactions.

3.       If balance change is bigger than configured limit, then change is denied (explained further below).

4.       If player is in blacklist, then change is denied (explained further below).

Configuration (balance change limit and player blacklist) must be taken from external source. This can be file, database, external component etc.
Client itself is a server that offers gameplay logic. Specific gameplay will not be implemented, client can just generate random balance updates. Specific communication protocol between client and server is not specified (custom protocol can be invented). Server must write proper log information, where at least IN/OUT per player must be grep’able.
Commands between servers:


client->server: username, transaction id, balance change

server->client: transaction id, error code, balance version, balance change, balance after change


###Database structure
PLAYER(USERNAME, BALANCE_VERSION, BALANCE)


###Documentation
· Describe shortly the implementation aspects.

· If some features are not implemented, point out the reasons.